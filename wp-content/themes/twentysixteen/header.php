<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<div class="site-inner">
		<div class="menu_top">
			<div class="container">
				<div class="clearfix">
					<div class="pull-left">
						<div id="top-bar-content">
							<span class="fa fa-phone" style="color:#000;margin-right:5px;"></span> 1-800-987-654 
							<span class="fa fa-envelope" style="color:#000;margin-left:20px;margin-right:5px;"></span> admin@total.com
						</div>
					</div>
				</div>
			</div>
		</div>
		<header class="navbar navbar-static-top menu_primary" id="top">
			<div class="container">
				<div class="navbar-header">
					<button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="" class="navbar-brand text-uppercase">
						<img src="<?= get_template_directory_uri() ?>/image/logo.png" alt="">
					</a>
				</div>
				<nav class="collapse navbar-collapse" id="bs-navbar">
					<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="#" class="">Về chúng tôi</a>
						</li>
						<li>
							<a href="#" class="">Dịch vụ</a>
						</li>
						<li>
							<a href="#" class="">Sản phẩm</a>
						</li>
						<li>
							<a href="#" class="">Tin tức</a>
						</li>
						<li>
							<a href="#" class="">Liên hệ</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>
