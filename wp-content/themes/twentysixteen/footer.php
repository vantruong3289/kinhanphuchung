			<div class="calling">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<h4 class="">
								<i class="fa fa-map-marker" aria-hidden="true"></i> Hãy liên hệ với chúng tôi bất cứ lúc nào. Văn phòng Hà Nội 123 - Nguyễn Trãi - Thành Xuân
							</h4>
						</div>
						<div class="col-md-2">
						</div>
					</div>
				</div>
			</div>
			<div class="footer">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<h3>
								<img src="<?= get_template_directory_uri() ?>/image/logo.png" alt="">
							</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla hendrerit nisl a ullamcorper pretium. Duis aliquet, lacus nec faucibus placerat, enim nibh iaculis lacus, eu varius nisl ligula ac lorem.
							</p>
						</div>
						<div class="col-sm-6 col-md-4">
							<h3>Recent News</h3>
							<ul>
								<li>
									<a href="#"><i class="fa fa-angle-right"></i> Don’t Just Stand There, Do Something!</a>
								</li>
								<li>
									<a href="#"><i class="fa fa-angle-right"></i> Award Winning Video by One Ocean, One Breath</a>
								</li>
								<li>
									<a href="#"><i class="fa fa-angle-right"></i> My Very First Ultra Marathon</a>
								</li>
								<li>
									<a href="#"><i class="fa fa-angle-right"></i> Mobile Friendly Design</a>
								</li>
							</ul>
						</div>
						<div class="col-sm-6 col-md-4">
							<h3>Contact Us</h3>
							<p>Phone: (555) 555-5555 </p>
							<p>Email: email@total.com  </p>
							<p>Facebook: @envato</p>
							<p>Address: 212 Hoàng Văn Thái , Khương Đình , Thanh Xuân, Hà Nội.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container text-center">
					<span>Copyright 2013 - All Rights Reserved</span>
				</div>
			</div>
		</div>
	</div><!-- .site-inner -->
</div><!-- .site -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>
