<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="crumb">
			<div class="container">
				<ol class="breadcrumb">
				  	<li><a href="#">Home</a></li>
				  	<li><a href="#">Library</a></li>
				  	<li class="active">Data</li>
				</ol>
			</div>
		</div>
		<div class="map">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<h2>Get In Touch With Us Today</h2>
						<p>
							You’ve got questions, and we have answers. 
							Just send us a message and one of our knowledgeable support staff will be in contact with you within 48hrs – even on weekends and holidays.
						</p>
					</div>
					<div class="col-sm-6 col-md-8">
						<form action="">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<textarea name="" class="form-control" rows="5"></textarea>
									</div>
								</div>
								<div class="col-sm-12">
									<button class="btn btn-block btn-primary">Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<div class="text-center">
							<h4>Địa chỉ</h4>
							<p>
								Our doors, ears and break room are always open (or at least Monday-Friday, from 9am-5pm).
							</p>
							<hr>
							<p>
								1234 Main Street<br>Anytown, USA 56789
							</p>
							<hr>
							<p><a href="#"><i class="fa fa-phone"></i> (555) 555-5555</a></p>
							<p><a href="#"><i class="fa fa-print"></i> (555) 555-5555</a></p>
						</div>
					</div>
					<div class="col-sm-6 col-md-8">
						<div>
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d103067.93942737603!2d-115.1764946138916!3d36.16964235561963!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80beb782a4f57dd1%3A0x3accd5e6d5b379a3!2sLas+Vegas%2C+NV!5e0!3m2!1sen!2sus!4v1539851087451" width="100%" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
			<p></p>
		</div>
	</main><!-- .site-main -->
</div><!-- .content-area -->
<?php get_footer(); ?>
