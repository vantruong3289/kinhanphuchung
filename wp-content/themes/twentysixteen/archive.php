<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main category">
			<div class="crumb">
				<div class="container">
					<ol class="breadcrumb">
					  	<li><a href="#">Home</a></li>
					  	<li><a href="#">Library</a></li>
					  	<li class="active">Data</li>
					</ol>
				</div>
			</div>
			<div class="posts">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<div class="thumbnail">
								<div class="row">
									<div class="col-sm-6">
										<a href="#">
											<img src="http://totaltheme.wpengine.com/base/wp-content/uploads/sites/7/drag-drop-ft.jpg" class="img-responsive center-block">
										</a>
									</div>
									<div class="col-sm-6">
										<h3 class="text-center">Drag & Drop Builder</h3>
										<p>
											Easily build and save layouts using a drag & drop page builder. Create unlimited layouts for all your posts or pages with just a few clicks.
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-6">
							<div class="thumbnail">
								<div class="row">
									<div class="col-sm-6">
										<a href="#">
											<img src="http://totaltheme.wpengine.com/base/wp-content/uploads/sites/7/drag-drop-ft.jpg" class="img-responsive center-block">
										</a>
									</div>
									<div class="col-sm-6">
										<h3 class="text-center">Drag & Drop Builder</h3>
										<p>
											Easily build and save layouts using a drag & drop page builder. Create unlimited layouts for all your posts or pages with just a few clicks.
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-6">
							<div class="thumbnail">
								<div class="row">
									<div class="col-sm-6">
										<a href="#">
											<img src="http://totaltheme.wpengine.com/base/wp-content/uploads/sites/7/drag-drop-ft.jpg" class="img-responsive center-block">
										</a>
									</div>
									<div class="col-sm-6">
										<h3 class="text-center">Drag & Drop Builder</h3>
										<p>
											Easily build and save layouts using a drag & drop page builder. Create unlimited layouts for all your posts or pages with just a few clicks.
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-6">
							<div class="thumbnail">
								<div class="row">
									<div class="col-sm-6">
										<a href="#">
											<img src="http://totaltheme.wpengine.com/base/wp-content/uploads/sites/7/drag-drop-ft.jpg" class="img-responsive center-block">
										</a>
									</div>
									<div class="col-sm-6">
										<h3 class="text-center">Drag & Drop Builder</h3>
										<p>
											Easily build and save layouts using a drag & drop page builder. Create unlimited layouts for all your posts or pages with just a few clicks.
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
