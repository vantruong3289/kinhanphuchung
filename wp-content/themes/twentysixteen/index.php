<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="silde">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      <img src="<?= get_template_directory_uri() ?>/image/03.jpg" alt="" class="img-responsive center-block">
				      <div class="carousel-caption">
				        	<h3>Về mặt ngôn ngữ, Hồng Phúc có nghĩa là Phúc lớn.</h4>
							<p>Với chúng tôi, Hồng Phúc có nghĩa là Hạnh Phúc, Ấm áp và Thịnh vượng!</p>
				      </div>
				    </div>
				    <div class="item">
				      <img src="<?= get_template_directory_uri() ?>/image/04.jpg" alt="" class="img-responsive center-block">
				      <div class="carousel-caption">
				        	<h3>Về mặt ngôn ngữ, Hồng Phúc có nghĩa là Phúc lớn.</h4>
							<p>Với chúng tôi, Hồng Phúc có nghĩa là Hạnh Phúc, Ấm áp và Thịnh vượng!</p>
				      </div>
				    </div>
				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
			<div class="service">
				<div class="container">
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<div class="">
								<div class="">
									<a href="#">
										<h2 class="text-center"><i class="fa fa-life-ring" aria-hidden="true"></i> Tư vấn &amp; hỗ trợ</h2>
									</a>
									<div class="text-center">
										<p>Not only do you get a&nbsp;clean and well coded WordPress theme, but also the support you deserve.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="">
								<div class="">
									<a href="#">
										<h2 class="text-center"><span class="fa fa-cubes" aria-hidden="true"></span> Thiết kế &amp; Thi công</h2>
									</a>
									<div class="text-center">
										<p>Total was developed with child theming in mind, but there are also tons of built-in options for customizing it.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="">
								<div class="">
									<a href="#">
										<h2 class="text-center"><span class="fa fa-diamond" aria-hidden="true"></span> Năng động &amp; Chất lượng</h2>
									</a>
									<div class="text-center">
										<p>Total comes with an easy drag and drop editor via the Visual Composer making it easy to setup new sites in no time!</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="top">
				<div class="container">
					<h2 class="text-center">
						<span></span>An Phuc Hung Glass<span></span>
					</h2>
					<p class="text-center">
						Về mặt ngôn ngữ, Hồng Phúc có nghĩa là Phúc lớn.<br>
						Với chúng tôi, Hồng Phúc có nghĩa là Hạnh Phúc, Ấm áp và Thịnh vượng!
					</p>
				</div>
				<img src="<?= get_template_directory_uri() ?>/image/09.jpg" class="center-block img-responsive" style="min-height: 240px;">
			</div>
			<div class="new">
				<div class="container">
					<h2 class="text-center">
						<span></span> Thành quả đạt được <span></span>
					</h2>
					<p class="text-center">
						Bằng kinh nghiệm, chất lượng dịch vụ. Nhiều chung cư, văn phòng cao cấp đều có sản phẩm kính chúng tôi.<br>
						Niềm tự hào về thành quả đạt được trên mọi miền tỉnh thành đất nước.
					</p>
					<div class="lists">
						<div class="row">
							<div class="col-sm-6 col-md-4">
								<img src="http://totaltheme.wpengine.com/base/wp-content/uploads/sites/7/dragdrop-new.jpg" alt="" class="center-block">
								<h3 class="">TimeCity &amp; VinGroup</h3>
								<p class="text-center">
									Easily build and save layouts using a drag & drop page builder. Create unlimited layouts for all your posts and pages, no need to memorize a bunch of shortcodes.
								</p>
							</div>
							<div class="col-sm-6 col-md-4">
								<img src="http://totaltheme.wpengine.com/base/wp-content/uploads/sites/7/responsive-new.jpg" alt="" class="center-block">
								<h3 class="">Hotel FLC</h3>
								<p class="text-center">
									Easily build and save layouts using a drag & drop page builder. Create unlimited layouts for all your posts and pages, no need to memorize a bunch of shortcodes.
								</p>
							</div>
							<div class="col-sm-6 col-md-4">
								<img src="http://totaltheme.wpengine.com/base/wp-content/uploads/sites/7/options-new.jpg" alt="" class="center-block">
								<h3 class="">Royal &amp; VinGroup</h3>
								<p class="text-center">
									Easily build and save layouts using a drag & drop page builder. Create unlimited layouts for all your posts and pages, no need to memorize a bunch of shortcodes.
								</p>
							</div>
							<div class="col-sm-6 col-md-4">
								<img src="http://totaltheme.wpengine.com/base/wp-content/uploads/sites/7/browser-new.jpg" alt="" class="center-block">
								<h3 class="">Hotel Mường thanh</h3>
								<p class="text-center">
									Easily build and save layouts using a drag & drop page builder. Create unlimited layouts for all your posts and pages, no need to memorize a bunch of shortcodes.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="quanlity">
				<div class="container">
					<h2 class="text-center">Chất lượng từ quy trình hiệu quả</h2>
					<p class="text-center">
						Có kế hoạch , định hướng cụ thể trước khi bắt đầu.<br>
						Từng bước triển khai hiệu quả mang lại kết quả tốt nhất.
					</p>
					<div class="glass">
						<div class="row">
							<div class="col-sm-6 col-md-4 col-md-offset-1">
								<img src="<?= get_template_directory_uri() ?>/image/light-phone.png">
							</div>
							<div class="col-sm-6 col-md-7">
								<div class="">
									<div class="item">
										<div class="vcex-icon-box-icon">
											<span class="fa fa-cubes" aria-hidden="true"></span>
										</div>
										<h3 class="vcex-icon-box-heading">Xây dựng thiết kế</h3>
										<div class="vcex-icon-box-content clr">
											<p>Total has been coded for optimum speed. Scripts are loaded only when necessary and the code is optimized to output as little and as slim as possible to ensure your site is fast.</p>
										</div>
									</div>
									<div class="item">
										<div class="vcex-icon-box-icon">
											<span class="fa fa-life-ring" aria-hidden="true"></span>
										</div>
										<h3 class="vcex-icon-box-heading">Tư vấn chọn kính</h3>
										<div class="vcex-icon-box-content clr">
											<p>Watch your Google rankings grow with Total. This theme has been SEO optimized and uses the best practices to ensure your on-page SEO is up to par. The built-in breadcrumbs are also coded to display on Google snippets.</p>
										</div>
									</div>
									<div class="item">
										<div class="vcex-icon-box-icon">
											<i class="fa fa-tasks" aria-hidden="true"></i>
										</div>
										<h3 class="vcex-icon-box-heading">Lên kế hoạch</h3>
										<div class="vcex-icon-box-content clr">
											<p>With over 300 theme options (and counting) this WordPress theme makes it easier then ever to tweak and better fit your client needs without having to get down and dirty with the code.</p>
										</div>
									</div>
									<div class="item">
										<div class="vcex-icon-box-icon">
											<i class="fa fa-truck" aria-hidden="true"></i>
										</div>
										<h3 class="vcex-icon-box-heading">Thi công</h3>
										<div class="vcex-icon-box-content clr">
											<p>The only way you’ll get the support you deserve from a theme is if the support is coming straight from the developer. We made the theme so we know how it works and can truly help you with any issues.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="products">
				<div class="container">
					<h2 class="text-center"><span></span> Sản phẩm kính ưa thích <span></span> </h2>
					<p class="text-center">
						Có rất nhiều khách hàng đã thể hiện sự yêu thích với sản phẩm kính<br>
					</p>
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<div class="product">
								<div class="">
									<a href="" class="">
										<img class="img-responsive" src="<?= get_template_directory_uri() ?>/image/06.jpg" >
									</a>
								</div>
								<div class="product-text">
									<h2 class="text-center">
										<a href="http://totaltheme.wpengine.com/base/portfolio-item/down-the-road/">Kính Văn Phòng</a>
									</h2>
									<p class="text-center">
										Donec semper erat ligula, eget fringilla lacus ultricies rhoncus. Phasellus ac iaculis tellus, ac purus. Aenean vulputate turpis.
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="product">
								<div class="">
									<a href="" class="">
										<img class="img-responsive" src="<?= get_template_directory_uri() ?>/image/07.jpg" >
									</a>
								</div>
								<div class="product-text">
									<h2 class="text-center">
										<a href="http://totaltheme.wpengine.com/base/portfolio-item/down-the-road/">Kính Cửa</a>
									</h2>
									<p class="text-center">
										Donec semper erat ligula, eget fringilla lacus ultricies rhoncus. Phasellus ac iaculis tellus, ac purus. Aenean vulputate turpis.
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="product">
								<div class="">
									<a href="" class="">
										<img class="img-responsive" src="<?= get_template_directory_uri() ?>/image/08.jpg" >
									</a>
								</div>
								<div class="product-text">
									<h2 class="text-center">
										<a href="http://totaltheme.wpengine.com/base/portfolio-item/down-the-road/">Kính Trang Trí</a>
									</h2>
									<p class="text-center">
										Donec semper erat ligula, eget fringilla lacus ultricies rhoncus. Phasellus ac iaculis tellus, ac purus. Aenean vulputate turpis.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
